#ifndef RENDERAPI_H
#define RENDERAPI_H

#include <glad/glad.h>

#include <algorithm>
#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>
#include <iostream>
#include <memory>
#include <vector>

#define GLSL(src) "#version 330 core\n" #src

template <int Type>
class GlShader
{
public:
    GlShader(const char *source)
    {
        _index = glCreateShader(Type);

        glShaderSource(_index, 1, &source, NULL);
        glCompileShader(_index);

        GLint result = GL_FALSE;
        GLint logLength;

        glGetShaderiv(_index, GL_COMPILE_STATUS, &result);
        if (result == GL_FALSE)
        {
            glGetShaderiv(_index, GL_INFO_LOG_LENGTH, &logLength);
            std::vector<char> error(static_cast<size_t>((logLength > 1) ? logLength : 1));
            glGetShaderInfoLog(_index, logLength, NULL, &error[0]);

            std::cerr << "tried compiling shader\n\t" << source << "\n\n"
                      << "got error\n\t" << error.data() << std::endl;

            glDeleteShader(_index);

            _index = 0;
        }
    }

    ~GlShader()
    {
        if (is_good())
        {
            glDeleteShader(_index);

            _index = 0;
        }
    }

    bool is_good() const { return _index > 0; }

private:
    GLuint _index = 0;

    friend class GlProgram;
};

class GlProgram
{
public:
    GlProgram()
    {
        _index = glCreateProgram();
    }

    ~GlProgram()
    {
        if (is_good())
        {
            glDeleteProgram(_index);
            _index = 0;
        }
    }

    template <int Type>
    void attach(const GlShader<Type> &shader)
    {
        glAttachShader(_index, shader._index);
    }

    void link()
    {
        glLinkProgram(_index);

        GLint result = GL_FALSE;
        GLint logLength;

        glGetProgramiv(_index, GL_LINK_STATUS, &result);
        if (result == GL_FALSE)
        {
            glGetProgramiv(_index, GL_INFO_LOG_LENGTH, &logLength);
            std::vector<char> error(static_cast<size_t>((logLength > 1) ? logLength : 1));
            glGetProgramInfoLog(_index, logLength, NULL, &error[0]);

            std::cerr << "tried linking program, got error:\n"
                      << error.data();
        }
    }

    GLint getAttribLocation(const char *name) const { return glGetAttribLocation(_index, name); }
    GLint getUniformLocation(const char *name) const { return glGetUniformLocation(_index, name); }

    void use() const { glUseProgram(_index); }

    bool is_good() const { return _index > 0; }

private:
    GLuint _index = 0;
};

class RenderApi
{
    struct Vertex
    {
        float pos[3];
        float color[4];
        float uv[2];
    };

    struct Face
    {
        size_t firstVertex;
        size_t vertexCount;
        bool fan = true;
    };

    struct Mesh
    {
        unsigned int textureIndex;
        size_t firstFace;
        size_t faceCount;
    };

public:
    void Setup()
    {
        glGenVertexArrays(1, &_vao);
        glGenBuffers(1, &_vbo);

        glBindVertexArray(_vao);
        glBindBuffer(GL_ARRAY_BUFFER, _vbo);
        glBufferData(GL_ARRAY_BUFFER, _vertices.size() * sizeof(float), _vertices.data(), GL_STATIC_DRAW);

        GlShader<GL_VERTEX_SHADER> vs(GLSL(
            layout(location = 0) in vec3 a_position;
            layout(location = 1) in vec4 a_color;
            layout(location = 2) in vec2 a_uv;

            uniform mat4 u_matrix;

            out vec4 f_color;
            out vec2 f_uv;

            void main() {
                gl_Position = u_matrix * vec4(a_position.xyz, 1.0);
                f_color = a_color;
                f_uv = a_uv;
            }));

        GlShader<GL_FRAGMENT_SHADER> fs(GLSL(
            uniform sampler2D u_tex0;

            in vec4 f_color;
            in vec2 f_uv;

            out vec4 color;

            void main() {
                vec4 texel0;
                texel0 = texture2D(u_tex0, f_uv);
                color = vec4(texel0.rgb, 1.0) * f_color;
            }));

        _program = std::unique_ptr<GlProgram>(new GlProgram());

        _program->attach(vs);
        _program->attach(fs);
        _program->link();

        _program->use();

        auto a_position = _program->getAttribLocation("a_position");
        glVertexAttribPointer(a_position, 3, GL_FLOAT, GL_FALSE, sizeof(float) * 9, (void *)0);
        glEnableVertexAttribArray(a_position);

        auto a_color = _program->getAttribLocation("a_color");
        glVertexAttribPointer(a_color, 4, GL_FLOAT, GL_FALSE, sizeof(float) * 9, (void *)(sizeof(float) * 3));
        glEnableVertexAttribArray(a_color);

        auto a_uv = _program->getAttribLocation("a_uv");
        glVertexAttribPointer(a_uv, 2, GL_FLOAT, GL_FALSE, sizeof(float) * 9, (void *)(sizeof(float) * 7));
        glEnableVertexAttribArray(a_uv);

        auto u_tex0 = _program->getUniformLocation("u_tex0");
        glUniform1i(u_tex0, 0);
    }

    void BeginFrame()
    {
        _vertices.clear();
        _faces.clear();
        _meshes.clear();
    }

    void Render(
        const glm::mat4 &proj)
    {
        if (_vertices.empty())
        {
            return;
        }

        _program->use();

        glBindVertexArray(_vao);
        glBindBuffer(GL_ARRAY_BUFFER, _vbo);
        glBufferData(GL_ARRAY_BUFFER, _vertices.size() * sizeof(float), _vertices.data(), GL_STATIC_DRAW);

        glm::mat4 view;
        view = glm::lookAt(glm::vec3(50.0f, -10.0f, 10.0f),
                           glm::vec3(0.0f, 0.0f, 0.0f),
                           glm::vec3(0.0f, 0.0f, 1.0f));

        unsigned int transformLoc = _program->getUniformLocation("u_matrix");
        glUniformMatrix4fv(transformLoc, 1, GL_FALSE, glm::value_ptr(proj * view));

        for (auto &mesh : _meshes)
        {
            glActiveTexture(GL_TEXTURE0);
            glBindTexture(GL_TEXTURE_2D, mesh->textureIndex);

            for (size_t f = mesh->firstFace; f < mesh->faceCount; f++)
            {
                auto &face = _faces[f];
                if (face->fan)
                {
                    glDrawArrays(GL_TRIANGLE_FAN, face->firstVertex, face->vertexCount);
                }
                else
                {
                    glDrawArrays(GL_TRIANGLE_STRIP, face->firstVertex, face->vertexCount);
                }
            }
        }
    }

    void Texture(unsigned int index)
    {
        if (_currentMesh == nullptr)
        {
            return;
        }

        _currentMesh->textureIndex = index;
    }

    void BeginMesh()
    {
        if (_currentMesh != nullptr)
        {
            return;
        }

        _currentMesh = std::unique_ptr<Mesh>(new Mesh());
        _currentMesh->firstFace = _faces.size();
    }

    void EndMesh()
    {
        if (_currentMesh == nullptr)
        {
            return;
        }

        _currentMesh->faceCount = _faces.size() - _currentMesh->firstFace;

        _meshes.push_back(std::move(_currentMesh));
        _currentMesh = nullptr;
    }

    void BeginFace(bool fan)
    {
        if (_currentFace != nullptr)
        {
            return;
        }

        _currentFace = std::unique_ptr<Face>(new Face());
        _currentFace->firstVertex = (_vertices.size() / 9);
        _currentFace->fan = fan;
    }

    void EndFace()
    {
        if (_currentFace == nullptr)
        {
            return;
        }

        _currentFace->vertexCount = (_vertices.size() / 9) - _currentFace->firstVertex;

        _faces.push_back(std::move(_currentFace));
        _currentFace = nullptr;
    }

    void Position(const glm::vec3 &pos)
    {
        _vertices.push_back(pos[0]);
        _vertices.push_back(pos[1]);
        _vertices.push_back(pos[2]);

        _vertices.push_back(_nextColor[0]);
        _vertices.push_back(_nextColor[1]);
        _vertices.push_back(_nextColor[2]);
        _vertices.push_back(_nextColor[3]);

        _vertices.push_back(_nextUv[0]);
        _vertices.push_back(_nextUv[1]);
    };

    void Color(const glm::vec4 &color)
    {
        _nextColor = color;
    };

    void Uv(const glm::vec2 &uv)
    {
        _nextUv = uv;
    };

private:
    unsigned int _vao;
    unsigned int _vbo;
    std::unique_ptr<GlProgram> _program;
    int _positionAttrib;
    int _colorAttrib;
    int _uvAttrib;

    unsigned int VertexSize() const { return sizeof(Vertex); }

    glm::vec4 _nextColor;
    glm::vec2 _nextUv;
    std::unique_ptr<Face> _currentFace = nullptr;
    std::unique_ptr<Mesh> _currentMesh = nullptr;

    std::vector<float> _vertices;
    std::vector<std::unique_ptr<Face>> _faces;
    std::vector<std::unique_ptr<Mesh>> _meshes;
};

#endif // RENDERAPI_H
