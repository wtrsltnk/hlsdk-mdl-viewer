#include <app.hpp>
#include <glad/glad.h>
#include <iostream>
#define _USE_MATH_DEFINES
#include <cmath>

#include "studiomodel.h"

vec3_t g_vright; // needs to be set to viewer's right in order for chrome to work
float g_lambert = 1.5;

static StudioModel *tempmodel = nullptr;
static StudioEntity *tempentity = nullptr;

void gluPerspective(GLdouble fovy, GLdouble aspect, GLdouble zNear, GLdouble zFar)
{
    GLdouble xmin, xmax, ymin, ymax;

    ymax = zNear * tan(fovy * M_PI / 360.0);
    ymin = -ymax;
    xmin = ymin * aspect;
    xmax = ymax * aspect;

    glFrustum(xmin, xmax, ymin, ymax, zNear, zFar);
}

//////////////////////////////////////////////////////////////////

void App::OnInit()
{
    tempmodel = new StudioModel();
    tempmodel->Init("C:\\Games\\Counter-Strike1.3\\cstrike\\models\\player\\sas\\sas.mdl");

    tempentity = new StudioEntity(tempmodel);
    tempentity->SetSequence(0);
    tempentity->SetController(0, 0.0);
    tempentity->SetController(1, 0.0);
    tempentity->SetController(2, 0.0);
    tempentity->SetController(3, 0.0);
    tempentity->SetMouth(0);
    tempentity->SetAngles(235, 0, -90);
    tempentity->SetOrigin(0, 0, -50);
    tempentity->SetBlending(0, 0.0);
    tempentity->SetBlending(1, 0.0);

    tempentity->AdvanceFrame(0.01f);

    tempentity->DrawModel(_renderer);

    glActiveTexture(GL_TEXTURE0);
    _renderer.Setup();

    glClearColor(0.0f, 0.0f, 0.5f, 0.0f);
}

void App::OnFrame()
{
    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

    glViewport(0, 0, _width, _height);

    glCullFace(GL_FRONT);
    glEnable(GL_DEPTH_TEST);

    glDepthFunc(GL_LEQUAL);
    glDepthRange(0.0, 10.0);
    glDepthMask(1);

    tempentity->AdvanceFrame(0.01f);

    _renderer.BeginFrame();

    tempentity->DrawModel(_renderer);

    _renderer.Render(_proj);
}

void App::OnResize(int width, int height)
{
    if (height <= 0)
    {
        height = 1;
    }

    _width = width;
    _height = height;

    _proj = glm::perspective(glm::radians(70.0f), float(_width) / float(_height), 0.1f, 1300.0f);
}

void App::OnExit()
{}
