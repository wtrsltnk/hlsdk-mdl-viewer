#include <app.hpp>

#include <config.h>
#include <glad/glad.h>
#include <glad/glad_wgl.h>
#include <iostream>
#include <memory>

#define OPENGL_LATEST_VERSION_MAJOR 4
#define OPENGL_LATEST_VERSION_MINOR 6

struct WindowHandle
{
    HWND hwnd;
    HDC hdc;
    HGLRC hrc;
};

template <class T>
T *App::GetWindowHandle() const
{
    return reinterpret_cast<T *>(_windowHandle);
}

template <class T>
void App::SetWindowHandle(T *handle)
{
    _windowHandle = (void *)handle;
}

void App::ClearWindowHandle()
{
    _windowHandle = nullptr;
}

App::App(const std::vector<std::string> &args)
    : _args(args)
{}

App::~App() = default;

LRESULT CALLBACK WndProc(HWND hwnd, UINT message, WPARAM wParam, LPARAM lParam)
{
    auto app = reinterpret_cast<App *>(GetWindowLongPtr(hwnd, GWLP_USERDATA));

    switch (message)
    {
        case WM_NCCREATE:
        {
            SetWindowLongPtr(hwnd, GWLP_USERDATA, (LONG_PTR)((LPCREATESTRUCT)lParam)->lpCreateParams);
            break;
        }
        case WM_SIZE:
        {
            if (app != nullptr)
            {
                app->OnResize(LOWORD(lParam), HIWORD(lParam));
            }

            break;
        }
        case WM_DESTROY:
        {
            if (app != nullptr)
            {
                app->OnExit();

                auto windowHandle = app->GetWindowHandle<WindowHandle>();

                if (windowHandle != nullptr)
                {
                    wglDeleteContext(windowHandle->hrc);
                    ReleaseDC(windowHandle->hwnd, windowHandle->hdc);
                }
            }

            PostQuitMessage(0);

            break;
        }
        case WM_CLOSE:
        {
            DestroyWindow(hwnd);

            break;
        }
    }

    return DefWindowProc(hwnd, message, wParam, lParam);
}

void OpenGLMessageCallback(
    unsigned source,
    unsigned type,
    unsigned id,
    unsigned severity,
    int length,
    const char *message,
    const void *userParam)
{
    (void)userParam;
    switch (severity)
    {
        case GL_DEBUG_SEVERITY_HIGH:
            std::cout << "CRITICAL";
            break;
        case GL_DEBUG_SEVERITY_MEDIUM:
            std::cout << "ERROR";
            break;
        case GL_DEBUG_SEVERITY_LOW:
            std::cout << "WARNING";
            break;
        case GL_DEBUG_SEVERITY_NOTIFICATION:
            std::cout << "DEBUG";
            break;
        default:
            std::cout << "UNKNOWN";
            break;
    }

    std::cout << "\n    source    : " << source
              << "\n    message   : " << message
              << "\n    type      : " << type
              << "\n    id        : " << id
              << "\n    length    : " << length
              << "\n";
}

bool App::Init()
{
    WNDCLASS wc;
    ZeroMemory(&wc, sizeof wc);

    wc.hInstance = GetModuleHandle(nullptr);
    wc.lpszClassName = APP_NAME;
    wc.lpfnWndProc = (WNDPROC)WndProc;
    wc.style = CS_DBLCLKS | CS_VREDRAW | CS_HREDRAW | CS_OWNDC;
    wc.hbrBackground = nullptr;
    wc.lpszMenuName = nullptr;
    wc.hIcon = LoadIcon(nullptr, IDI_WINLOGO);
    wc.hCursor = LoadCursor(nullptr, IDC_ARROW);

    if (FALSE == RegisterClass(&wc))
    {
        return false;
    }

    auto _hwnd = CreateWindowEx(
        WS_EX_APPWINDOW | WS_EX_WINDOWEDGE,
        APP_NAME,
        APP_NAME,
        WS_OVERLAPPEDWINDOW | WS_CLIPSIBLINGS | WS_CLIPCHILDREN,
        CW_USEDEFAULT,
        CW_USEDEFAULT,
        1200,
        1200,
        0,
        0,
        GetModuleHandle(nullptr),
        reinterpret_cast<LPVOID>(this));

    if (_hwnd == nullptr)
    {
        return false;
    }

    auto _hdc = GetWindowDC(_hwnd);

    if (_hdc == nullptr)
    {
        DestroyWindow(_hwnd);

        return false;
    }

    PIXELFORMATDESCRIPTOR pfd;

    ZeroMemory(&pfd, sizeof(pfd));
    pfd.nSize = sizeof(pfd);
    pfd.nVersion = 1;
    pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
    pfd.iPixelType = PFD_TYPE_RGBA;
    pfd.cColorBits = 24;
    pfd.cDepthBits = 24;

    int format = ChoosePixelFormat(_hdc, &pfd);

    if (format == 0)
    {
        ReleaseDC(_hwnd, _hdc);
        DestroyWindow(_hwnd);

        return false;
    }

    if (SetPixelFormat(_hdc, format, &pfd) == FALSE)
    {
        ReleaseDC(_hwnd, _hdc);
        DestroyWindow(_hwnd);

        return false;
    }

    auto _hrc = wglCreateContext(_hdc);

    if (_hrc == NULL)
    {
        ReleaseDC(_hwnd, _hdc);
        DestroyWindow(_hwnd);

        return false;
    }

    wglMakeCurrent(_hdc, _hrc);

    gladLoadGL();

    ShowWindow(_hwnd, SW_SHOW);

    if (GLVersion.major >= 3)
    {
        int attribs[] =
            {
                WGL_CONTEXT_MAJOR_VERSION_ARB,
                OPENGL_LATEST_VERSION_MAJOR,
                WGL_CONTEXT_MINOR_VERSION_ARB,
                OPENGL_LATEST_VERSION_MINOR,
                0,
            };

        auto _createContextAttribs = (PFNWGLCREATECONTEXTATTRIBSARBPROC)wglGetProcAddress("wglCreateContextAttribsARB");

        if (_createContextAttribs != nullptr)
        {
            auto tempContext = _createContextAttribs(_hdc, 0, attribs);

            if (tempContext != nullptr)
            {
                wglMakeCurrent(nullptr, nullptr);
                wglDeleteContext(_hrc);

                _hrc = tempContext;
                wglMakeCurrent(_hdc, _hrc);

                gladLoadGL();
            }
        }

        glEnable(GL_DEBUG_OUTPUT);
        glEnable(GL_DEBUG_OUTPUT_SYNCHRONOUS);
        glDebugMessageCallback(OpenGLMessageCallback, nullptr);

        glDebugMessageControl(
            GL_DONT_CARE,
            GL_DONT_CARE,
            GL_DEBUG_SEVERITY_NOTIFICATION,
            0,
            nullptr,
            GL_FALSE);
    }

    std::cout << "running opengl " << GLVersion.major << "." << GLVersion.minor << std::endl;

    SetWindowHandle<WindowHandle>(new WindowHandle({
        _hwnd,
        _hdc,
        _hrc,
    }));

    OnInit();

    return true;
}

int App::Run()
{
    MSG msg;
    bool running = true;

    auto windowHandle = std::unique_ptr<WindowHandle>(GetWindowHandle<WindowHandle>());

    if (windowHandle == nullptr)
    {
        std::cout << "Something went wrong, did you forget to call the Init() method?" << std::endl;

        return 0;
    }

    while (running)
    {
        while (PeekMessage(&msg, nullptr, 0, 0, PM_REMOVE))
        {
            if (msg.message == WM_QUIT)
            {
                running = false;
            }

            TranslateMessage(&msg);
            DispatchMessage(&msg);
        }

        wglMakeCurrent(windowHandle->hdc, windowHandle->hrc);

        OnFrame();

        SwapBuffers(windowHandle->hdc);
    }

    ClearWindowHandle();

    return 0;
}
