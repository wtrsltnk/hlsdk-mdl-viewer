# mdl-viewer

Simple MDL file viewer project with the latest OpenGL using a load from https://glad.dav1d.de/. Most of the code is borrowed from Valve's Half-Life SDK.

![Screenshot](screenshot.png)
