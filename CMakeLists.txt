cmake_minimum_required(VERSION 3.12)

project(mdl-viewer VERSION "0.1.0")

include(cmake/CPM.cmake)
include(cmake/Dependencies.cmake)

configure_file(config.h.in config.h)

add_executable(mdl-viewer
    #src/mdlviewer.cpp
    include/app.hpp
    include/renderapi.hpp
    src/app-infra.cpp
    src/app.cpp
    src/common/cmdlib.c
    src/common/cmdlib.h
    src/common/mathlib.c
    src/common/mathlib.h
    src/engine/studio.h
    src/glad.c
    src/glad_wgl.c
    src/program.cpp
    src/studio_render.cpp
    src/studio_utils.cpp
    src/studiomodel.h
    src/renderapi.cpp
)

target_compile_features(mdl-viewer
    PRIVATE
        cxx_nullptr
)

target_include_directories(mdl-viewer
    PRIVATE
        "include"
        "${PROJECT_BINARY_DIR}"
)

find_package(OpenGL REQUIRED)

target_link_libraries(mdl-viewer
    PRIVATE
        ${OPENGL_LIBRARIES}
        glm
)
